# Solution Building Blocks for POC

## Table of Contents
* Introduction
* Building Block Template (Use to document new blocks)
* General Building Blocks
* Proof of Concept Building Blocks

# Instruction
Please keep all building blocks short and precise like the example below.
You may link to additional information stored elsewhere in the repository.


# Building Block Template

***
1. **Building block name**: Solution for creating a new software project.
1. **Functionality provided**: The building block enables solutions to standardize project creation.
1. **Link to example implementation or interfaces**:
https://start.spring.io
1. **Outstanding work to complete this building block:** Implement docker containers to standardize runing Spring-CLI.
1. **Architectural alignment**: This building block enables or reflects the following business objectives and principles:
   * Objective 1: *xxxxx*
   * Principle 1: *yyyy*
   * Objective 2: *xxxxx*
   * Principle 2: *yyyy*
***

## General Building Blocks


***
1. **Building block name**: 
1. **Functionality provided**: 
1. **Link to example implementation or interfaces**:

1. **Outstanding work to complete this building block:** 
1. **Architectural alignment**: 
   * Objective 1:
   * Principle 1: 

***



## Proof of Concept Building Blocks

***
1. **Building block name**:   
Structuring the project
1. **Functionality provided**:  
- Repository Java 17, Spring Boot 3.0
- Pipeline CI/CD for test, build and quality analysis
1. **Link to example implementation or interfaces**:
- https://start.spring.io/
- https://docs.gitlab.com/ee/ci/
- https://www.baeldung.com/jacoco
- https://sonarcloud.io/

***
1. **Building block name**: 
Creation of the domain layer
1. **Functionality provided**: 
It must provide everything link to the domain and the business logic
1. **Link to example implementation or interfaces**:
- Repository on folder domain
- It should contains Entity, ValueObject, Business Service, Exceptions, etc.

***
1. **Building block name**: 
Creation of the infrastructure layer
1. **Functionality provided**: 
It must provide fixtures, external api access
1. **Link to example implementation or interfaces**:
- Repository on folder infrastructure
- It should contains Fixtures, Tools, External api access

***
1. **Building block name**: 
Creation of the application layer
1. **Functionality provided**: 
It must provide DTO, Controller, HTTP Exception.
1. **Link to example implementation or interfaces**:
- Repository on folder application
- It should contains everything related to the REST

***
1. **Building block name**: 
Audit Security & Tests
1. **Functionality provided**: 
Certifies compliance with KPIs
1. **Link to example implementation or interfaces**:
***